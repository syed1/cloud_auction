import matplotlib.pyplot as plt
import numpy as np

def parse_log_file(filename):
    with open(filename) as f:
        timeline = dict()
        for line in f:
            data  = dict()
            if "WARNING" in line:
                #<time> WARNING key1:val1,key2:val2 ....
                vals = line.split()
                time = float(vals[0])
                data_str = vals[2]
                for key_val_str in data_str.split(','):
                    key, val = key_val_str.split(':')
                    data[key] = float(val)
            
                timeline[time] = data
        return timeline 

def plot_vs_time(timeline, data_key, label):
    x = list()
    y = list()
    for key in sorted(timeline.keys()):
        data = timeline[key]
        if  data_key in data and data[data_key] > 0:
            x.append(key) #time
            y.append(data[data_key]) #value
   
    print " %s Avg: %s : %f" %(label, data_key, np.mean(y))

    plt.axis([0,max(x)+5, 0, max(y)+5])
    return plt.plot(x,y, label=label)

def _sum_revenue(timeline):
    prev_rev = 0 
    for key in sorted(timeline.keys()):
        timeline[key]["cur_revenue"] += prev_rev
        prev_rev = timeline[key]["cur_revenue"] 

    return timeline

def cum_rev_time():
    legends = list()

    tl = _sum_revenue(parse_log_file("sim_list.log"))
    legends.append(plot_vs_time(tl, "cur_revenue","list")[0])
    print "list max: %s" % ( max(tl.values(), key=lambda x: x['cur_revenue']))


    tl = _sum_revenue(parse_log_file("sim_hybrid.log"))
    legends.append(plot_vs_time(tl, "cur_revenue", "hybrid")[0])
    print "hybrid max: %s" % (max(tl.values(), key=lambda x: x['cur_revenue']))

    tl = _sum_revenue(parse_log_file("sim_auction.log"))
    legends.append(plot_vs_time(tl, "cur_revenue", "auction")[0])
    print "auction max: %s" % (max(tl.values(), key=lambda x: x['cur_revenue']))


    plt.legend(handles=legends)
    plt.xlabel("Time(m)")
    plt.ylabel("Revenue")
    plt.show()



def rev_vs_time():
    legends = list()

    tl = parse_log_file("sim_list.log") 
    legends.append(plot_vs_time(tl, "cur_revenue","list")[0])

    tl = parse_log_file("sim_hybrid.log") 
    legends.append(plot_vs_time(tl, "cur_revenue", "hybrid")[0])

    tl = parse_log_file("sim_auction.log") 
    legends.append(plot_vs_time(tl, "cur_revenue", "auction")[0])


    plt.legend(handles=legends)
    plt.xlabel("Time(m)")
    plt.ylabel("Revenue")
    plt.show()


if __name__ == "__main__":
    #tl = parse_log_file("sim.log")
    #plot_vs_time(tl, "cur_revenue")
    #plot_vs_time(tl, "cur_cap")
    #rev_vs_time()
    cum_rev_time()
