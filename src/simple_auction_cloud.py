from __future__ import  print_function
from configparser import ConfigParser
from delayed_timeout import DelayedTimeout
from logger import logger
import collections
import simpy
import random 

config = ConfigParser()
config.read('sim.ini')

random.seed(42)

"""
Simulating a cloud for listed users 
Users who pay a *listed price* every hour

"""


class AuctionCloudController(object):


    def __init__(self, config,env):
        # cloud controller acts like a semaphore
        # so that only one user gets access
        # to update the cloud
        
        self.env = env
        self.controller = simpy.Resource(env, capacity = 1)
        self.max_capacity = int(config['cloud']['capacity'])
        self.cur_cap = 0
        self.busy_users = list()
        self.queued_users = list()

        self.cur_auction_price = float(config['auction']['min_auction_price'])
        self.min_auction_price = float(config['auction']['min_auction_price'])
        self.auction_period = float(config['auction']['auction_period'])


        self.max_user_time = float(config['user']['max_user_time'])
        self.min_user_time = float(config['user']['min_user_time'])
        
        self.commit_time = int(config['cloud']['min_commit_time'])
        self.no_act = 0
    

    def start_auction(self):
        """ 
        Auction happens every hour:
        
        find the K top bidders, find all users who are not in this list
        currently using the cloud and have passed their commit threshold
        replace them with better users. set the bid price to the lowest one

        """

        while True:
            with self.controller.request() as req:
                yield req


                revenue = 0
                for u in self.busy_users:
                    # started beyond last hour
                    if u.value['start_time'] < self.env.now + 60:
                        revenue += self.cur_auction_price
                    else:
                        revenue += (flaot(self.env.now - u.value['start_time'])/60)*self.cur_auction_price

                data = "cur_cap:%d,"%self.cur_cap +\
                   "max_cap:%d,"%self.max_capacity +\
                   "cur_revenue:%f,"% revenue  +\
                   "cur_auction_price:%f" % self.cur_auction_price
            
                logger.warn(data)

                logger.info("AUCTION START ")
    
                # remove all the busy users who have
                # not yet completed commit time
    
                busy_commit_done = filter(
                        lambda x: env.now - x.value['start_time'] >= self.commit_time, 
                        self.busy_users
                )
    
                busy_commited = filter(
                        lambda x: env.now - x.value['start_time'] < self.commit_time, 
                        self.busy_users
                )
                
                commit_user_count = len(self.busy_users) - len(busy_commit_done)
    
                auction_users = busy_commit_done + self.queued_users
                logger.debug(
                        "Busy commit done users:" + str(busy_commit_done)
                ) 
                 
                # sort by decreasing amount of bids

                sorted_users = sorted(
                        auction_users, 
                        key = lambda u: u.value['bid'],
                        reverse = True
                )
           
                # K slots for auction
                K =  int(self.max_capacity - len(busy_commited))
                # find the price
                if K <= len(sorted_users) - 1:
                    # more users than capacity, select top K
                    self.cur_auction_price = sorted_users[K].value['bid']
                    selected_users = sorted_users[:K] 
                    preemted_users = filter(
                            lambda x: x in self.busy_users,
                            sorted_users[K:]
                    ) 

                else:
                    # users less than capacity, price = min price 
                    self.cur_auction_price = self.min_auction_price 
                    selected_users = sorted_users 
                    preemted_users = []
 
 
                #stop the preemted uses
                for user in preemted_users:
                    self.busy_users.remove(user)
                    # create a new user 
                    # with same values because 
                    # using the same user causes problem 
                    # with double event trigger
                    newuser = self.create_user(
                            self.env,
                            user.value['name'],
                            user.value['bid'],
                            user.value['arrive_time'],
                            user.value['use_time']
                    )

                    self.queued_users.append(newuser)
                    self.stop_user(user)
                    self.cur_cap-=1


                # remove them from wait and add them to busy
                # and start them
                for user in selected_users:
                    if user in self.queued_users:
                        self.queued_users.remove(user)
                        assert(user not in self.busy_users)
                        self.busy_users.append(user)
                        self.start_user(user)
                        assert(self.cur_cap < self.max_capacity)
                        self.cur_cap+=1


        
                self.print_current_users()

            if len(self.queued_users) + len(self.busy_users)  <= 0:
                self.no_act += 1
            else:
                self.no_act = 0

            
            if self.no_act < 3:
                yield self.env.timeout(self.auction_period)
            else:
                env.exit()
    

    def req_controller(self, env, name, bid, user_type):
        """
        Called when a new request is to be made
        """

        logger.debug("VM-request name:%s, bid %f" % ( 
                    name,
                    bid
            ))


        with self.controller.request() as req:
            yield req # sleep until we get the controller
            
            arrive = env.now

            #random VM use time
            if user_type == "listed":
                vm_use_time = random.uniform(self.min_user_time, self.max_user_time)*100
            else:
                vm_use_time = random.uniform(self.min_user_time, self.max_user_time)


            u = self.create_user(env, name, bid, arrive, vm_use_time)
            
            # if bid > cur_price and capacity available, start them
            if bid >= self.cur_auction_price and self.cur_cap < self.max_capacity:
                self.busy_users.append(u)
                assert(self.cur_cap < self.max_capacity)
                self.cur_cap+=1
                self.start_user(u)

            elif bid >= self.min_auction_price:
                self.queue_user(u)

            else:
                logger.debug("Bid price less than lowest reserve")
                env.exit()


    def release_controller(self, user):
        """ 
        Called when a user is done (timed out)
        """
        #if user.value['name'] == 'U-18-57':
        #    import ipdb; ipdb.set_trace()

        if user in self.busy_users:
            logger.info(" User " + user.value['name'] + " leaving")

            self.busy_users.remove(user)
            self.cur_cap-=1


        elif user in self.queued_users:
            self.queued_users.remove(user)
        
        else:
            assert("Zombie user" == "")

        self.print_current_users()


    def create_user(self, env, name, bid, arrive_time, use_time):

        user = DelayedTimeout(
                env,
                value= {
                    'name': name,
                    'bid': bid,
                    'arrive_time': arrive_time, 
                    'start_time' : None,
                    'use_time': use_time
                },
                delay=use_time
        )
        return user

    
    def start_user(self, user):
        #if user.value['name'] == 'U-18-57':
        #    import ipdb; ipdb.set_trace()

        logger.debug("Starting user %s delay %d" % (user.value['name'], user.delay))
        user.value['start_time'] = self.env.now
        user.callbacks = list()
        user.callbacks.append(self.release_controller)
        user.start()

    def stop_user(self, user):
        logger.debug("Stopping user %s" % user.value['name'])

        user.value['arrive_time'] = self.env.now
        user.value['start_time'] = None
        user.callbacks.remove(self.release_controller)
   
    def preemt_handler(self, user):
        pass 

    def queue_user(self, user):
        self.queued_users.append(user)
    
    def print_current_users(self):
        logger.debug("Current users: ")

        logger.debug("\tBUSY")
        for user in self.busy_users:
            logger.debug(
                "\tN:%s, B:%f, U:%s"%(
                    user.value['name'], 
                    user.value['bid'], 
                    user.value['use_time']
            ))

        logger.debug("\tQUEUED")
        for user in self.queued_users:
            logger.debug(
                "\tN:%s, B:%f, U:%s"%(
                    user.value['name'], 
                    user.value['bid'], 
                    user.value['use_time']
            ))

env = simpy.Environment() 
logger.set_env(env)

def req_gen(env):
    sim_hours = 10
    for hour in range(sim_hours):
        # randomly choose a rate for
        # this hour, the rate will
        # be chosen from a normal 
        # distribution with a 
        # mean 

        num_vms_auction = 10 # no of vm requests every hour TODO:random
        num_vms_listed = 1 # no of vm requests every hour TODO:random

        times_to_start_auction = random.sample(range(1,61), num_vms_auction)
        times_to_start_listed = random.sample(range(1,61), num_vms_listed)

        for i in range(1,61):
            if i in times_to_start_auction:
                bid = random.uniform(0,1)
                env.process(ctrl.req_controller(
                    env, 
                    "A-%d-%d"%(hour,i), 
                    bid,
                    'auction'
                ))
            if i in times_to_start_listed:
                bid = 1 # price
                env.process(ctrl.req_controller(
                    env, 
                    "A-%d-%d"%(hour,i), 
                    bid,
                    'listed'
                ))

            yield env.timeout(1)

ctrl = AuctionCloudController(config, env)
#start the auction

env.process(ctrl.start_auction())
env.process(req_gen(env))

env.run()
#cloud 
