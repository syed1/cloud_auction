from __future__ import  print_function

from configparser import ConfigParser
from delayed_timeout import DelayedTimeout

import collections
import simpy
import random 

config = ConfigParser()
config.read('sim.ini')

from logger import logger


"""
Simulating a cloud for listed users 
Users who pay a *listed price* every hour

"""


class ListCloudController(object):


    def __init__(self, config, env):
        # cloud controller acts like a semaphore
        # so that only one user gets access
        # to update the cloud

        self.controller = simpy.Resource(env, capacity = 1)
        self.max_capacity = int(config['cloud']['capacity'])
        self.cur_free_cap = int(config['cloud']['capacity'])
        self.tot_capacity = 0
        self.cur_list_cap = 0
        self.list_users = list()
        self.env = env
        self.price = 1
        self.cur_alloc_failures = 0

        self.max_user_time = float(config['user']['max_user_time'])
        self.min_user_time = float(config['user']['min_user_time'])

        self.auction_period = float(config['auction']['auction_period'])
        self.no_act = 0
    
    def perf_mon(self):

        """
        Since this simulation doesn't run 
        periodic auctions, we run perf_mon
        every hour to spew stats about 
        the current state of the cloud
        """

        while True:
            with self.controller.request() as req:
                yield req


                revenue = 0
                for u in self.list_users:
                    if u.value['start_time'] < self.env.now + 60:
                        revenue += 1
                    else:
                        revenue += flaot(self.env.now - u.value['start_time'])/60

                data = "cur_cap:%d,"%self.cur_list_cap +\
                       "max_cap:%d,"%self.max_capacity +\
                       "cur_revenue:%d,"% revenue + \
                       "alloc_failures:%d" % self.cur_alloc_failures
                
                logger.warn(data)
                self.cur_alloc_failures  = 0 #reset for next hour

            if len(self.list_users)  <= 0:
                self.no_act += 1
            else:
                self.no_act = 0

            if self.no_act < 3:
                yield self.env.timeout(self.auction_period)
            else:
                env.exit()
 

        

    def req_controller(self, env, name, price):
        """
        Called when a new request is to be made
        """
        #random VM use time
        vm_use_time = random.uniform(self.min_user_time, self.max_user_time)


        with self.controller.request() as req:
            yield req # sleep until we get the controller
            
            arrive = env.now

            if self.tot_capacity >= self.max_capacity:
                logger.critical("%7.4f %s: not enough capacity for listed users" % (arrive, name) )
                self.cur_alloc_failures += 1
                en.exit()
            
           
            logger.info('%7.4f %s: got the VM' % (arrive, name))
            u = self.create_user(env, name, price, arrive, vm_use_time)
            self.start_user(u)

            self.tot_capacity +=1
            self.cur_list_cap +=1
            self.cur_free_cap -=1
            self.list_users.append(u)

            self.print_current_users()


    def release_controller(self, user):
        """ 
        Called when a user is done
        """
        with self.controller.request() as req:
            logger.info("%7.4f User " % env.now  + user.value['name'] + " leaving")
            self.list_users.remove(user)
            self.tot_capacity -=1
            self.cur_list_cap -=1
            self.cur_free_cap +=1

            self.print_current_users()


    def create_user(self, env, name, bid, arrive_time, use_time):

        user = DelayedTimeout(
                env,
                value= {
                    'name': name,
                    'bid': bid,
                    'arrive_time': arrive_time, 
                    'start_time' : None,
                    'use_time': use_time
                },
                delay=use_time
        )
        return user

    def start_user(self, user):
        user.value['start_time'] = self.env.now
        user.callbacks.append(self.release_controller)
        # start the delayed timer
        user.start()

    
    def print_current_users(self):
        logger.debug("Current users: ")
        for user in self.list_users:
            logger.debug(
                "\tN:%s, B:%f, U:%s"%(
                    user.value['name'], 
                    user.value['bid'], 
                    user.value['use_time']
            ))

env = simpy.Environment() 
logger.set_env(env)
ctrl = ListCloudController(config, env)

def req_gen(env):
    """
    Function to generate users:


    """

    # No of hours to simulate 
    sim_hours = 10 
    for hour in range(sim_hours):
        # randomly choose a rate for
        # this hour, the rate will
        # be chosen from a normal 
        # distribution with a 
        # mean 

        num_vms = 3 # no of vm requests this hour
        times_to_start = random.sample(range(1,61), num_vms)

        for i in range(1,61):
            if i in times_to_start:
                env.process(ctrl.req_controller(env, "U-%d-%d"%(hour,i), 1))
            yield env.timeout(1)
    

env.process(ctrl.perf_mon())
env.process(req_gen(env))
env.run()

#cloud 
