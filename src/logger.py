from logging.config import fileConfig
from colorlog import ColoredFormatter
import logging 
import simpy


class CloudLogger():
    def __init__(self):
        self._logger = logging.getLogger()
        fileConfig("sim.ini")

    def debug(self, message):
        self._logger.debug(
            message, 
            extra={"simtime": self.env.now}
        )

    def info(self, message):
        self._logger.info(
            message, 
            extra={"simtime": self.env.now}
        )

    def warn(self, message):
        self._logger.warn(
            message, 
            extra={"simtime": self.env.now}
        )

    def error(self, message):
        self._logger.error(
            message, 
            extra={"simtime": self.env.now}
        )

    def fatal(self, message):
        self._logger.fatal(
            message, 
            extra={"simtime": self.env.now}
        )

    def critical(self, message):
        self._logger.critical(
            message, 
            extra={"simtime": self.env.now}
        )


    def set_env(self, env):
        self.env = env

logger = CloudLogger()


if __name__ == "__main__":
    
    env = simpy.Environment()
    logger.set_env(env)    
    # Some examples.
    logger.debug("this is a debugging message")
    logger.info("this is an informational message")
    logger.warn("this is a warning message")
    logger.error("this is an error message")
    logger.fatal("this is a fatal message")
    logger.critical("this is a critical message")
