from __future__ import  print_function
from configparser import ConfigParser
from delayed_timeout import DelayedTimeout
from logger import logger
import collections
import simpy
import random 

config = ConfigParser()
config.read('sim.ini')


"""
Simulating a cloud for listed users 
Users who pay a *listed price* every hour

"""


class AuctionCloudController(object):


    def __init__(self, config,env):
        # cloud controller acts like a semaphore
        # so that only one user gets access
        # to update the cloud
        
        self.env = env
        self.controller = simpy.Resource(env, capacity = 1)
        self.max_capacity = int(config['cloud']['capacity'])
        self.cur_cap = 0
        self.busy_users = list()
        self.queued_users = list()
        self.list_users = list()

        self.cur_auction_price = float(config['auction']['min_auction_price'])
        self.min_auction_price = float(config['auction']['min_auction_price'])
        self.auction_period = float(config['auction']['auction_period'])


        self.max_user_time = float(config['user']['max_user_time'])
        self.min_user_time = float(config['user']['min_user_time'])
        
        self.commit_time = int(config['cloud']['min_commit_time'])
        self.no_act = 0
    

    def start_auction(self):
        """ 
        Auction happens every hour:
        
        find the K top bidders, find all users who are not in this list
        currently using the cloud and have passed their commit threshold
        replace them with better users. set the bid price to the lowest one

        """

        while True:
            with self.controller.request() as req:
                yield req

                revenue = 0
                for u in self.busy_users:
                    # started beyond last hour
                    if u.value['start_time'] < self.env.now + 60:
                        revenue += self.cur_auction_price
                    else:
                        revenue += (flaot(self.env.now - u.value['start_time'])/60)*self.cur_auction_price

                for u in self.list_users:
                    if u.value['start_time'] < self.env.now + 60:
                        revenue += u.value['bid'] #price
                    else:
                        revenue += (flaot(self.env.now - u.value['start_time'])/60)*u.value['bid']


                data = "cur_cap:%d,"%self.cur_cap +\
                   "max_cap:%d,"%self.max_capacity +\
                   "cur_revenue:%f,"% revenue  +\
                   "cur_auction_price:%f" % self.cur_auction_price
            
                logger.warn(data)



                logger.info("AUCTION START ")
    
                # remove all the busy users who have
                # not yet completed commit time
    
                busy_commit_done = filter(
                        lambda x: env.now - x.value['start_time'] >= self.commit_time, 
                        self.busy_users
                )
    
                busy_commited = filter(
                        lambda x: env.now - x.value['start_time'] < self.commit_time, 
                        self.busy_users
                )
                
                commit_user_count = len(self.busy_users) - len(busy_commit_done)
    
                auction_users = busy_commit_done + self.queued_users
                logger.debug("Busy commit done users:" + str(busy_commit_done)) 
                 
                # sort by decreasing amount of bids

                sorted_users = sorted(
                        auction_users, 
                        key = lambda u: u.value['bid'],
                        reverse = True
                )
            
                # find the price
                K =  int(self.max_capacity - len(busy_commited) - len(self.list_users))

               
                if K <= len(sorted_users) - 1:
                    # more users than capacity, select top K
                    self.cur_auction_price = sorted_users[K].value['bid']
                    selected_users = sorted_users[:K] 
                    preemted_users = filter(
                            lambda x: x in self.busy_users,
                            sorted_users[K:]
                    ) 

                else:
                    # users less than capacity, price = min price 
                    self.cur_auction_price = self.min_auction_price 
                    selected_users = sorted_users 
                    preemted_users = []


                #stop the preemted users
                for user in preemted_users:
                    self.busy_users.remove(user)
                    newuser = self.create_user(
                            self.env,
                            user.value['name'],
                            user.value['bid'],
                            user.value['arrive_time'],
                            user.value['use_time'],
                            user.value['user_type']
                    )
                    self.queued_users.append(newuser)
                    self.stop_user(user)
                    self.cur_cap-=1

            
                # remove queued users from wait and add them to busy
                # and start them
                for user in selected_users:
                    if user in self.queued_users:
                        self.queued_users.remove(user)
                        assert(user not in self.busy_users)
                        self.busy_users.append(user)
                        self.start_user(user)
                        self.cur_cap+=1
        

                self.print_current_users()
                
            if len(self.queued_users) + len(self.busy_users)  + len(self.list_users) <= 0:
                self.no_act += 1
            else:
                self.no_act = 0

            
            if self.no_act < 3:
                yield self.env.timeout(self.auction_period)
            else:
                env.exit()
 
    

    def req_controller(self, env, name, bid, user_type):
        
        """
        Called when a new request is to be made
        can create an auction user or a bid user
        """

        with self.controller.request() as req:
            yield req #sleep until we get the controller
            
            arrive = env.now

            logger.debug("VM-request name:%s, type: %s, bid %f" % ( 
                    name,
                    user_type,
                    bid
            ))


            if user_type == "listed":
                vm_use_time = random.uniform(self.min_user_time, self.max_user_time)*100
            else:
                vm_use_time = random.uniform(self.min_user_time, self.max_user_time)


            u = self.create_user(env, name, bid, arrive, vm_use_time, user_type)
            if user_type == 'listed':
                logger.debug("cur cap:%d, max_cap %d" %( self.cur_cap, self.max_capacity))
                if self.cur_cap >= self.max_capacity:
                    # try to preemt some auction users
                    logger.debug("Extra capacity, cleanin up")
                    busy_commit_done = filter(
                            lambda x: 
                                env.now - x.value['start_time'] >= self.commit_time, 
                            self.busy_users
                    )

                    busy_commit_done = sorted(
                        busy_commit_done, 
                        key=lambda x: x.value['bid'],
                        reverse=True
                    )

                    if busy_commit_done:
                        preemted_user = busy_commit_done[-1]
                        newuser = self.create_user(
                                self.env,
                                preemted_user.value['name'],
                                preemted_user.value['bid'],
                                preemted_user.value['arrive_time'],
                                preemted_user.value['use_time'],
                                preemted_user.value['user_type']
                        )

                        self.busy_users.remove(preemted_user)
                        self.queued_users.append(newuser)
                        self.stop_user(preemted_user)
                        self.cur_cap-=1

                    else:
                        logger.critical("%7.4f %s: not enough capacity for listed users" % (arrive, name) )
                        env.exit()

                u = self.create_user(env, name, bid, arrive, vm_use_time, user_type)
                self.cur_cap +=1
                self.list_users.append(u)
                self.start_user(u)
                logger.info('got the VM %s , cur_cap %d' % (name, self.cur_cap))
                assert(self.cur_cap <= self.max_capacity)

            elif user_type == "auction":
                # if bid > cur_price and capacity available, start them

                if bid > self.cur_auction_price and self.cur_cap < self.max_capacity:
                    self.busy_users.append(u)   
                    self.cur_cap+=1
                    self.start_user(u)
                else:
                    self.queue_user(u)

            self.print_current_users()


    def release_controller(self, user):
        """ 
        Called when a user is done
        """
        if user in self.busy_users:
            self.busy_users.remove(user)
            self.cur_cap-=1

        elif user in self.queued_users:
            self.queued_users.remove(user)

        elif user in self.list_users:
            self.list_users.remove(user)
            self.cur_cap-=1

        else:
            assert("Zombie user" == "")
 
        self.print_current_users()


    def create_user(self, env, name, bid, arrive_time, use_time, user_type):

        user = DelayedTimeout(
                env,
                value= {
                    'name': name,
                    'bid': bid,
                    'arrive_time': arrive_time, 
                    'start_time' : None,
                    'use_time': use_time,
                    'user_type': user_type
                },
                delay=use_time
        )
        return user

    
    def start_user(self, user):
        logger.debug(user.callbacks)
        user.value['start_time'] = self.env.now
        user.callbacks = list()
        user.callbacks.append(self.release_controller)
        user.start()

    def stop_user(self, user):
        logger.debug("Stopping user %s" % user.value['name'])
        user.value['arrive_time'] = self.env.now
        user.value['start_time'] = None
        user.callbacks.remove(self.release_controller)
        user.callbacks.append(self.preemt_handler)
   
    def preemt_handler(self, user):
        logger.debug("PREEMT HANDLER")
        logger.debug(user.value)
        logger.debug(user.callbacks)


    def queue_user(self, user):
        self.queued_users.append(user)
    
    def print_current_users(self):
        logger.debug("Current users: ")

        logger.debug("\tLISTED")
        for user in self.list_users:
            logger.debug(
                "\tN:%s, B:%f, U:%s"%(
                    user.value['name'], 
                    user.value['bid'], 
                    user.value['use_time'])
            )


        logger.debug("\tBUSY")
        for user in self.busy_users:
            logger.debug(
                "\tN:%s, B:%f, U:%s"%(
                    user.value['name'], 
                    user.value['bid'], 
                    user.value['use_time'])
            )

        logger.debug("\tQUEUED")
        for user in self.queued_users:
            logger.debug(
                "\tN:%s, B:%f, U:%s"%(
                    user.value['name'], 
                    user.value['bid'], 
                    user.value['use_time']
            ))

env = simpy.Environment() 
logger.set_env(env)

def req_gen(env):
    
    sim_hours = 10
    for hour in range(sim_hours):
        # randomly choose a rate for
        # this hour, the rate will
        # be chosen from a normal 
        # distribution with a 
        # mean 

        num_vms_auction = 10 # no of vm requests every hour TODO:random
        num_vms_listed = 1 # no of vm requests every hour TODO:random

        times_to_start_auction = random.sample(range(1,61), num_vms_auction)
        times_to_start_listed = random.sample(range(1,61), num_vms_listed)

        for i in range(1,61):
            if i in times_to_start_auction:
                bid = random.uniform(0,1)
                env.process(ctrl.req_controller(
                    env, 
                    "A-%d-%d"%(hour,i), 
                    bid,
                    'auction'
                ))
            if i in times_to_start_listed:
                bid = 1 # price
                env.process(ctrl.req_controller(
                    env, 
                    "A-%d-%d"%(hour,i), 
                    bid,
                    'listed'
                ))

            yield env.timeout(1)
 
ctrl = AuctionCloudController(config, env)
#start the auction


env.process(ctrl.start_auction())
env.process(req_gen(env))

env.run()
#cloud 
