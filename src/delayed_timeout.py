from simpy.events import Event
from logger import logger


class DelayedTimeout(Event):
    def __init__(self, env, value, delay):
        self.env = env
        self._value = value
        self.delay = int(delay)
        self.callbacks = []
        self.ok = False

    def start(self):
        self.ok = True
        self.env.schedule(self, 1, self.delay)

