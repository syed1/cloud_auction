%\documentclass[conference]{IEEEtran}
\documentclass[a4paper,11pt]{article}
\usepackage[parfill]{parskip}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{algorithm}
\usepackage{amssymb}
%\usepackage{algorithm2e}
\usepackage{algpseudocode} 
\usepackage{caption}
\usepackage{subcaption}


\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{Resource Allocation Auction for Hybrid Clouds}



\author{Syed Mushtaq Ahmed}



% make the title area
\maketitle


\begin{abstract}
%\boldmath

Resource allocation and pricing in clouds is a open problem. With the demand for 
cloud resources increasing, cloud providers have economical incentive 
to look for mechanisms which maximize revenue . With the
exception of Amazon, all the other cloud provider offer a fixed-price based
pricing model which leads to the underutilization of the
cloud \cite{Amazon2010, Agmon2013}.  In our study, we evaluate three cloud pricing
mechanisms i.e fixed-price, pure auction and hybrid auction.  We find that
using a hybrid approach with fixed-price and generalized vickrey auction
mechanisms gives an average higher revenue than other methods.

\end{abstract}


\section{Introduction}

Cloud computing has become the mainstay for modern utility computing.
Organizations are migrating to the cloud because of its flexibility 
and economies of scale. Cloud computing has transformed the 
traditional software development arena as developers do not 
have to invest a significant amount of their budget on infrastructure
as they can scale as per demand. This has created a huge demand for cloud
providers. All major public/private cloud providers \cite{Prodan2009} provide 
virtual machines (VMs) of various dimensions in 
terms of CPU, Memory, Disk Capacity and other features. 
In terms of pricing,  current  policy is a very simple pay-as-you-go model
where each \emph{type} of VM has a fixed hourly rate and users by the number
of hours used. Though this is very simple, it becomes expensive for users
who want to run lot of VMs for a short amount of time. 

Cloud users can be broadly classified into two types: static users and batch
users. Static users are users who have a very low request rate but have a high
VM hold time. These are the users who use the VM to host a continuous process
like a web server.  Reliability is their primary objective as they cannot
afford downtime, hence, they are willing to pay more for a better SLA. On the
other hand job users have a high request rate but hold the VM for short
durations of time. They use the VM for jobs like map-reduce, video encoding or
other background processes.  Their primary objective is to minimize cost as
they can afford downtime.  In this study we try to find an allocation/pricing
policy which can maximize revenue for a mixture of such users

\section{Vickrey Clarke Groves mechanism} \label{gen_vickery}

To recall, consider an auction with $n$ bidders with each bidder having a
private value $v_i$ which he is willing to pay for the item and $b_i$ be his
actual bid. let $p$ be the price he pays if he wins the auction.  His utility
is given by 

\begin{equation*}
    u_i(b_i) = \begin{cases}
               v_i - p         & \text{if i gets the item}\\
               0               & \text{otherwise}\\
               \end{cases}
\end{equation*}

We want to give the item to the bidder with the maximum $v_i$ i.e 
winner $i = argmax_j(v_j)$ but since we only have access to their individual 
bids $b_i$, we should design an auction which should guarantee that the
item goes to the users who values it the most. In a vickrey auction, 
we give the item to the player with the highest bid and he will
pay the next highest bid as the price \cite{Vickrey1961}. We can prove that 
using this mechanism, it is optimal for bidders to 
bid their true value and manipulation can never increase
their utility.

Generalizing the vickrey mechanism where there are multiple identical items 
and each player can bid for more than one item, let us consider 
X as the set of all possible allocations for allocating
$k$ items between $n$ players. From \cite{Nisan2007}, a mechanism $(f, b_1, b_2, ..., b_n)$ is called a
Vickrey-Clarke-Groves(VCG) mechanism if

\begin{itemize}
\item{ $f(v_1, v_2..., v_n) \in argmax_{x \in X} \sum_i v_i(x) \text{ where } v_i(x)$ 
 is the value of user $i$ for allocation $x \in X$ i.e. the mechanism maximizes social welfare. }
\item{for some functions $h_1, h_2, ..., h_n, \text{where }  h_i: V_{-i} \rightarrow \mathbb{R} $ 
(i.e. $h_i$ does not depend on $v_i$), we have  $ \forall v_1 \in V_1 ... , v_n \in V_n:$
The price paid by each bidder is given by $p_i(v_1, ..., v_n) = h_i(v_{-i}) - \sum_{j \ne i} v_j(f(v_1, ..., v_n))$}
where $V \subseteq \mathbb{R}^X$ is the set of possible allocations for player $i$.
\end{itemize}

According to the Clarke pivot rule, the payment of a player $i$ is given by:
\begin{equation*}
p_i(v_1, ..., v_n)  = max_b \sum_{j \ne i} v_i(b) - \sum_{j \ne i} v_i(a)
\text{ where }  a = f(v_1, ..., v_n)
\end{equation*}

thus the payment for player $i$ is the loss caused by the player. It is the difference between
the social welfare with and without $i$ \cite{Groves1973}. 

In our study, we are restrict a user to bid for a single item only. In this
case the VCG mechanism translates to a uniform price auction. For $k$ identical
items (VMs), each user is interested in only one item. To maximize social
welfare in this case, we would allocate the $k$ items to the $k$ highest
bidders and charge them with the price of the ${k+1}^{th}$ bid.


\section{Cloud Allocation Algorithms}

In this section we describe the three algorithms which we simulate for our
analysis. All algorithms assume the time to provision a VM and cleanup of the
VM is instantaneous. We consider only a single type of VM and that a user
may request only one VM at one time. 


The following is the setup:
\begin{itemize}
    \item{$C_{total}$ is the total capacity of the cloud.}
    \item{$C_{busy}^{t}$ is the in-use capacity of the cloud at time $t$.}
    \item{$C_{listed}^{t}$ is the in-use capacity of the cloud by listed users at time $t$.}
    \item{$C_{auction}^{t}$ is the in-use capacity of the cloud by auction users at time $t$.}
    \item{$r_l$ rate of listed users per hour.}
    \item{$r_a$ rate of auction users per hour.}
    \item{$p_{listed}$ Listed price of the VM.}
    \item{$p_{minr}$ minimum reserve price for the auction.}
    \item{$p_r^{t}$ reserve price at time $t$.}
    \item{$b_i$ the bid for the $i^{th}$ user.}
    \item{$t_{arrival}^{i}$ time of arrival for the $i_{th}$ user.}
    \item{$t_{commit}$ commit time for auction users.}
    \item{$t_{now}$ current time.}
\end{itemize}

Two types of users can request cloud resources, listed and auction. 
Listed users arrive at a rate $r_l$ and pay the listed price $p_{listed}$
and the auction users arrive at a rate $r_a$ with a bid $b_i$. 


Once a user gets a VM via an auction, it cannot be forced to shut down until a
certain pre-determined amount of time called commit time ($t_{commit}$) has
elapsed.  This is necessary as this protects users from abrupt shutdown of
their VMs due to fluctuations in the reserve price frequently.



\subsection{Fixed-price allocation}

In this model, the cloud provider lists a single fixed price for the VM. The
users can reliably use the VM for as long as they need. If the cloud is out of
capacity, any request coming to allocate a new VM will be rejected. Algorithm
\ref{listed} shows the request handler for VMs. This is invoked every time a
new request comes from a user. 

In this case the revenue for each hour ($R^t$) is the product of the used
capacity and the price (we assume each VM occupies $1$ unit of the capacity).

\begin{equation*}
    R_{listed}^t = C_{busy}^{t} * p_{listed}
\end{equation*}

\begin{algorithm}
\caption{Listed request handler}\label{listed}

\begin{algorithmic}[1]
\If {$ C_{busy}^{t_{arrival}^{i} }\geq C_{total} $}
    \State \Return FAILURE 
\Else
    \State $U_{busy} \mathrel{+}= u_i$
    \State {$ C_{busy}^{t_{arrival}^{i}}  \mathrel{+}= 1 $} 
    \State \Return SUCCESS 
\EndIf
\end{algorithmic}
\end{algorithm}


\subsection{Pure auction allocation}

In this model all resources are allocated by an auction.  We use the generalized
Vickrey auction described in Section - \ref{gen_vickrey}.  There are two events
that happen. One is the request event which is triggered when a request for VM
allocation arrives. The other event is the auction which runs periodically.
During the request, if the cloud has enough capacity and the user's bid is more
than the current reserve, he gets the VM immediately. Otherwise, the user is
queued until the auction is run. 


During the auction, users whose commit time has not yet elapsed are filtered
out. The queued users along with the users who have crossed their commit 
time participate in the auction. They pay the price of the highest loser if
there are more requests than the capacity. If the number of users is less
than the current free capacity, they pay the minimum reserve price. After 
the auction the new reserve price is set accordingly. 

The hourly revenue is given by:

\begin{equation*}
    R_{auction}^t = C_{busy}^{t} * p_{a}^{t}
\end{equation*}


\begin{algorithm}
\caption{Auction request handler}\label{listed}
\begin{algorithmic}[1]
\Procedure{AuctionReq}{$u_i$} \Comment{user $u_i$ requesting a VM}
    \If { $b_i > p_{a}^{t}  \text{ and }  C_{busy}^{t_{arrival}^{i}} < C_{total} $ }
        %\State {$ C_{busy}^{t_{arrival}^{i}} \gets C_{busy}^{t_{arrival}^{i}} + 1 $} 
        \State $U_{busy} \mathrel{+}= u_i$
        \State {$ C_{busy}^{t_{arrival}^{i}}  \mathrel{+}= 1 $} 
        \State \Call{StartUser}{$u_i$}
        \State \Return SUCCESS
    \ElsIf{ $b_i > p_{a}^{t}$ }
        \State $U_{queue} \mathrel{+}= u_i$ \Comment{queue user $u_i$ }
        \State \Return PENDING
    \Else
        \State \Return FAILURE
    \EndIf
\EndProcedure
\end{algorithmic}
\end{algorithm}

\begin{algorithm}
\caption{Auction allocator}\label{listed}
\begin{algorithmic}[1]
\Procedure{AuctionAlloc}{} \Comment{This gets called periodically}
    \State $U_{comitted} =  \{ u_i \in U_{busy} | t_{arrival}^{i} < t_{now} + 60 \} $
    \State $U_{uncommited} =  \{ u_i \in U_{busy} | t_{arrival}^{i} \geq t_{now} + 60 \} $
   
    \State $U_{auction} = U_{uncomitted} + U_{queued}$
    \State $K = C_{total} - C_{busy}^{t_{now}}$

    \If { $K  < | U_{auction} |  $ } 
        \State $ U_{selected} = \{ \top K \text{ Users from } U_{auction} \}$
        \State $ U_{preemted} = \{ \bot X \text{ Users from } U_{auction} \text{ s.t } \forall x \in X, x \in U_{busy} \}$
        \State $p_{a}^{t} = U_{auction}[K+1]$
    \Else
        \State $U_{selected} = U_{auction}$ 
        \State $p_{a}^{t} = p_{minr}$
    \EndIf

    \ForAll{ $u_i \in U_{preemted} $}
        \State \Call{StopUser}{$u_i$}
        \State $U_{busy} \mathrel{-}= u_i$
        \State $U_{queued} \mathrel{+}= u_i$
        \State {$ C_{busy}^{t_{now}}  \mathrel{-}= 1 $} 
    \EndFor

    \ForAll{ $u_i \in U_{selected} $}
        \State \Call{StartUser}{$u_i$}
        \State $U_{busy} \mathrel{+}= u_i$
        \State {$ C_{busy}^{t_{now}}  \mathrel{+}= 1 $} 
    \EndFor
    \State \Return
\EndProcedure
\end{algorithmic}

\end{algorithm}

\subsection{Hybrid auction}

In the hybrid setup, different types of users are treated differently. Priority
is given to listed users as they will remain active for a long time and expect
reliability.  Auction users are guaranteed a minimum commit time. When a request
comes in, if there is capacity, regardless of type, it gets allocated. If there
is a capacity constraint and if the request is of listed type, we shut down an
auction instance and transfer that capacity to the listed VM. Otherwise, it is
an auction request and we queue it until we have capacity.

The available capacity during auction is calculated by removing the capacity occupied
by listed instances and auction users who are under their commit time.

The hourly revenue is given by
\begin{equation*}
    R_{hybrid}^t = C_{auction}^{t} * p_{a}^{t} + C_{listed} * p_{listed}
\end{equation*}


\begin{algorithm}
\caption{Hybrid request handler}\label{listed}
\begin{algorithmic}[1]
\Procedure{HybridReq}{$u_i$} \Comment{user $u_i$ requesting a VM}
    \If { type is $listed$ }
        \If { $ C_{busy}^{t_{now}} \geq C_{total}  $ }
            \State $U_{uncommited} =  \{ u_j \in U_{auction} | t_{arrival}^{j} \geq t_{now} + 60 \} $
            \If { $U_{uncommited}$ is empty}
                \State \Return FAILURE
            \Else
                \State $u_{preemted} = u_j \text{ s.t. }  \forall u^{'} \in U_{uncommited}, b^{'} > b_j $ 
                \State \Call{StopUser}{$u_{preemted}$}
                \State $U_{auction} \mathrel{-}= u_i$
                \State $U_{queued} \mathrel{+}= u_i$
                \State {$ C_{busy}^{t_{now}}  \mathrel{-}= 1 $} 
            \EndIf
        \EndIf
        
        \State $U_{listed} \mathrel{+}= u_i$
        \State \Call{StartUser}{$u_i$}
        \State \Return SUCCESS
    \Else \Comment{ it is an auction user }
        \If { $b_i > p_{a}^{t}  \text{ and }  C_{busy}^{t_{arrival}^{i}} < C_{total} $ }
            %\State {$ C_{busy}^{t_{arrival}^{i}} \gets C_{busy}^{t_{arrival}^{i}} + 1 $} 
            \State $U_{auction} \mathrel{+}= u_i$
            \State {$ C_{busy}^{t_{arrival}^{i}}  \mathrel{+}= 1 $} 
            \State \Call{StartUser}{$u_i$}
            \State \Return SUCCESS
        \ElsIf{ $b_i > p_{a}^{t}$ }
            \State $U_{queue} \mathrel{+}= u_i$ \Comment{queue user $u_i$ }
            \State \Return PENDING
        \Else
            \State \Return FAILURE
        \EndIf

    \EndIf
\EndProcedure
\end{algorithmic}
\end{algorithm}

\begin{algorithm}
\caption{Hybrid allocator}\label{listed}
\begin{algorithmic}[1]
\Procedure{HybridAlloc}{} \Comment{periodic allocator}
    \State $U_{comitted} =  \{ u_i \in U_{auction} | t_{arrival}^{i} < t_{now} + 60 \} $
    \State $U_{uncommited} =  \{ u_i \in U_{auction} | t_{arrival}^{i} \geq t_{now} + 60 \} $
   
    \State $U_{auction} = U_{uncomitted} + U_{queued}$
    \State $K = C_{total} - C_{busy}^{t_{now}} - C{listed}^{t_{now}}$

    \If { $K  < | U_{auction} |  $ } 
        \State $ U_{selected} = \{ \top K \text{ Users from } U_{auction} \}$
        \State $ U_{preemted} = \{ \bot X \text{ Users from } U_{auction} \text{ s.t } \forall x \in X, x \in U_{auction} \}$
        \State $p_{a}^{t} = U_{auction}[K+1]$
    \Else
        \State $U_{selected} = U_{auction}$ 
        \State $p_{a}^{t} = p_{minr}$
    \EndIf

    \ForAll{ $u_i \in U_{preemted} $}
        \State \Call{StopUser}{$u_i$}
        \State $U_{busy} \mathrel{-}= u_i$
        \State $U_{queued} \mathrel{+}= u_i$
        \State {$ C_{busy}^{t_{now}}  \mathrel{-}= 1 $} 
    \EndFor

    \ForAll{ $u_i \in U_{selected} $}
        \State \Call{StartUser}{$u_i$}
        \State $U_{busy} \mathrel{+}= u_i$
        \State {$ C_{busy}^{t_{now}}  \mathrel{+}= 1 $} 
    \EndFor
    \State \Return
\EndProcedure
\end{algorithmic}

\end{algorithm}

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth]{imgs/rev_vs_time}
                \caption{Hourly Revenue}
                \label{fig:rev}
        \end{subfigure}%
        ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
          %(or a blank line to force the subfigure onto a new line)
        \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth]{imgs/cum_rev_time}
                \caption{Cumilative Revenue}
                \label{fig:cum_rev}
        \end{subfigure}
        \caption{Revenue for various allocation algorithms}\label{fig:rev_curves}
\end{figure}



\section{Results}

We simulate the above algorithms using simpy which is a discrete event
simulator in python. A request generator randomly generates listed and auction
users with probability ratio $1:10$. Each request also carries the bid for that
request and the time it will use the VM for. The maximum (listed)  price for a
VM is $1.0$ and from \cite{} we can deduce that the minimum price should be
close to $0.1$.  We set this as our minimum reserve price. For each user, the
bid is chosen randomly from $U(0,1)$ and running time is chosen depending on
the type of the request. We keep the running time of a listed VM 100 times that
of a auction VM. The cloud has a capacity of 600 and the rate of requests is
set to $10 req/hour$ for auction users and $1 req/hr$ for listed users.  The
request initiation times are uniformly distributed over the next hour. We run
the request generator for 10 hours.

Figure \ref{fig:rev_curves} shows the revenue for the different allocation
strategies. Figure \ref{fig:rev_vs_time} shows the hourly revenue. Here we can
clearly see that We see that the hybrid mechanism clearly outperforms both
auction and listed mechanisms. Even though the auction mechanism runs for
longer time because of larger waiting, for a cloud with large capacity, this
does not matter. The problem with the pure auction mechanism is that the listed
users pay far less than their valuation thus reducing the revenue
significantly. This does not happen in the hybrid mechanism as they are
allocated at full price. The average hourly revenue for listed, auction and 
hybrid approaches were $4.9,1.2,5.0$ respectively and the cumulative 
revenue was $54,356.4,1299.0$ respectively.


We vary the auction:listed user ratio and the cloud capacity but find
the same result. A hybrid approach is the best way for a cloud providor to
maximize revenue. 

\section{Conclusion}

In this study we simulate three different allocation algorithms for cloud with
different user types with different requirement for workload and having different
valuation. We find that for mixed workloads, a hybrid allocation approach maximizes revenue and
since we are using VCG mechanism for the auction part, we are also guaranteed social welfare. 
Thus, a hybrid approach is optimal for both the cloud provider and user. 

There is still lot of scope for improvement. In this study we consider only a 
single kind of VM and assume that a user wants only one instance at a time 
which is not always the case. We also assume uniform distribution 
of bids and request time which also may change in a real world situation. 


\medskip

\bibliographystyle{alpha}%Used BibTeX style is unsrt
\bibliography{ref}



% that's all folks
\end{document}


